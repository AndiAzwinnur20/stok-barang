Ext.define('Stok.view.main.Form', {
    extend: 'Ext.form.Panel',
    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'form',
    id: 'form',
    items: [
                {
                    xtype: 'textfield',
                    name: 'nama_barang',
                    id: 'mnama',
                    label: 'Nama',
                    placeHolder: 'Your Name',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },{
                    xtype: 'textfield',
                    name: 'harga',
                    id: 'mharga',
                    label: 'Harga Barang',
                    placeHolder: '10000',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },{
                    xtype: 'textfield',
                    name: 'stok',
                    id: 'mstok',
                    label: 'Stok Barang',
                    placeHolder: '0',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'button',
                    ui: 'action',
                    text: 'Simpan',
                    handler: 'onSimpan'
                },
                {
                    xtype: 'button',
                    ui: 'confirm',
                    text: 'Tambahkan',
                    handler: 'onTambah'
                }
    ]
});