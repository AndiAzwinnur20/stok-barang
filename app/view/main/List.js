/**
 * This view is an example list of people.
 */
Ext.define('Stok.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        //'Stok.store.Personnel',
        'Ext.grid.plugin.Editable'
    ],
    plugins: [{
        type: 'grideditable'
    }],
    title: 'Stok Barang Bengkel',

    bind: '{personnel}',
    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    columns: [
        { text: 'Nama Barang', dataIndex: 'nama_barang',flex: 2, editable: true },
        { text: 'Harga Barang', dataIndex: 'harga', flex: 2, editable: true  },
        { text: 'Stok Barang', dataIndex: 'stok', flex: 2, editable: true  }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
