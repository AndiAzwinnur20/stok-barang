Ext.define('Stok.view.main.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'sdata',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Stok.store.Personnel',
        'Ext.field.Search'
    ],
    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [
        {
        xtype: 'dataview',
        scrollable: 'y',
        id: 'mydata',
        cls: 'ks-basic demo-solid-background',
        itemTpl:
        '<b>Nama Barang: {nama_barang}<b><br>' +
        'Harga Barang: {harga}<br>Stok Barang: {stok}<br>' +
        '<button type=button onClick="onDelete({kode_barang})">Hapus</button>' +
        '<button type=button onClick="onUpdate({kode_barang})">Edit</button><hr>',
        bind: {
            store: '{personnel}',
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',

            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: 'img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' +
                '<tr><td>Kode Barang:</td><td>{kode_barang}</td></tr>' +
                '<tr><td>Nama Barang:</td><td>{nama_barang}</td></tr>' +
                '<tr><td>Harga Barang:</td><td>{harga}</td></tr>' +
                '<tr><td>Stok Barang:</td><td>{stok}</td></tr>'
        }
    }]
});