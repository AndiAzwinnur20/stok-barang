/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Stok.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

   onItemSelected: function (sender, record) {
       Ext.getStore('detailstok').getProxy().setExtraParams({
           kode_barang: record.data.kode_barang
        });
        Ext.getStore('detailstok').load();
        //Ext.getStore('personnel').remove(record);
        //alert("data anda sudah dihapus");
    },


    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    onReadClicked: function () {
        Ext.getStore('personnel').load();
    },

    onReadDetailClicked: function () {
        Ext.getStore('detailstok').load();
    },

    onSimpan: function () {
        nama = Ext.getCmp('mnama').getValue();
        harga = Ext.getCmp('mharga').getValue();
        stok = Ext.getCmp('mstok').getValue();

        store = Ext.getStore('personnel');
        record = Ext.getCmp('mydata').getSelection();
        index = store.indexOf(record);
        record = store.getAt(index);
        store.beginUpdate();
        record.set('nama_barang', nama);
        record.set('harga', harga);
        record.set('stok', stok);
        store.endUpdate();
        alert("Updating...");
    },
    onTambah: function () {
        nama = Ext.getCmp('mnama').getValue();
        harga = Ext.getCmp('mharga').getValue();
        stok = Ext.getCmp('mstok').getValue();

        store = Ext.getStore('personnel');
        store.beginUpdate();
        store.insert(0, {'nama_barang':nama, 'harga':harga, 'stok':stok});
        store.endUpdate();
        alert("Inserting...");
    }
});

function onDelete(kode_barang) {
    record = Ext.getCmp('mydata').getSelection();
    Ext.getStore('personnel').remove(record);
    alert(kode_barang);
};

function onUpdate(kode_barang) {
    record = Ext.getCmp('mydata').getSelection();
    nama = record.data.nama_barang;
    harga = record.data.harga;
    stok = record.data.stok;
    Ext.getCmp('mnama').setValue(nama);
    Ext.getCmp('mharga').setValue(harga);
    Ext.getCmp('mstok').setValue(stok);
};
