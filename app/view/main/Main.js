/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Stok.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Stok.view.main.MainController',
        'Stok.view.main.MainModel',
        'Stok.view.main.List',
        'Stok.view.main.BasicDataView',
        'Stok.view.main.Form'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            docked: 'top',
            xtype: 'toolbar',
            items:[
                {
                    xtype: 'button',
                    text: 'Read Personnel',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onReadClicked'
                    }
                },{
                    xtype: 'button',
                    text: 'Read Detail',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onReadDetailClicked'
                    }
                }
            ]
        },{
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'panel',
                layout: 'hbox',
                items: [{
                    xtype: 'mainlist',
                    flex: 2
                }, {
                    xtype: 'sdata',
                    flex: 1
                }, {
                    xtype: 'form',
                    flex: 1
                }
                ]
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            items: [{

            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            bind: {
                html: '{loremIpsum}'
            }
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            bind: {
                html: '{loremIpsum}'
            }
        }
    ]
});
