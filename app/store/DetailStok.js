Ext.define('Stok.store.DetailStok', {
    extend: 'Ext.data.Store',
    alias: 'store.detailstok',
    storeId: 'detailstok',
    autoLoad: true,
    //autoSync: true,
    fields: [
        'kode_barang', 'nama_barang', 'harga', 'stok'
    ],

    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/Stok_Barang/readDetailStokBarang.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty: 'error'
        }
    }
});
