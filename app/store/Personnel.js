Ext.define('Stok.store.Personnel', {
    extend: 'Ext.data.Store',
    alias: 'store.personnel',
    storeId: 'personnel',
    autoLoad: true,
    autoSync: true,
    fields: [
        'kode_barang', 'nama_barang', 'harga', 'stok'
    ],

    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/Stok_Barang/readStokBarang.php",
            update: "http://localhost/Stok_Barang/updateStokBarang.php",
            destroy: "http://localhost/Stok_Barang/destroyStokBarang.php",
            create : "http://localhost/Stok_Barang/createStokBarang.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty: 'error'
        }
    },
    listeners: {
        beforeload: function(store, operation, e0pts){
            this.getProxy().setExtraParams({
                kode_barang: -1
            });
        }
    }
});
